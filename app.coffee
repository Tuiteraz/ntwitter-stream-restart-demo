#--( DEPENDENCIES
log          = require("helpers/winston-wrapper")(module)
config       = require "nconf"
express      = require "express"
bootable     = require "bootable"
bootableEnv  = require "bootable-environment"
_            = require "underscore"
http         = require "http"
requireTree  = require 'require-tree'
controllers  = requireTree 'controllers'

#--) DEPENDENCIES

#--- BODY --------------

app = bootable express()

app.phase bootable.initializers('setup/initializers/')
app.phase bootableEnv('setup/environments/',app)
app.phase bootable.routes('routes/all-route.js',app)

log.info "Creating app server.."
server = http.createServer app



log.info "Booting app.."
app.boot (err)->
  throw err if err

  server.listen config.get('express:port'), ()=>
    log.info "Express listen port ", config.get("express:port")

#    controllers.twitter.stream.getRateLimitStatus (jErr,hRes)->
#      log.info "rate limit status:#{JSON.stringify(hRes)}"
#
#    sStreamParams = JSON.stringify {
#      sTrack : "obama"
#      sCountryCode : "US"
#      sState : "New York"
#    }
    sStreamParams = JSON.stringify {
#      sTrack : "putin"
      sTrack : ""
#      sCountryCode : "RU"
      sLanguage : "ru"
    }

    controllers.twitter.stream.onNewQuery(sStreamParams,app)

    setTimeout ->
      app.jStream.destroy()
#      controllers.twitter.stream.getRateLimitStatus (jErr,hRes)->
#        log.info "rate limit status:#{JSON.stringify(hRes)}"
    , 7000

#    setTimeout ->
#      sStreamParams = JSON.stringify {
#        sTrack : "putin"
#        sCountryCode : "RU"
#        sLanguage : "ru"
#      }
#
#      controllers.twitter.stream.onNewQuery(sStreamParams,app)
#    , 11000
#
#    setTimeout ->
#      app.jStream.destroy()
#    , 17000


