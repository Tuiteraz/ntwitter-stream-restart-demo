// Generated by CoffeeScript 1.7.1
(function() {
  var bodyParser, config, express, expressLogger, log, path, rootDir, serveStatic;

  log = require("helpers/winston-wrapper")(module);

  config = require('nconf');

  express = require('express');

  expressLogger = require('../../middlewares/express-logger');

  bodyParser = require('body-parser');

  serveStatic = require('serve-static');

  path = require('path');

  rootDir = process.cwd();

  module.exports = function() {
    this.set('views', path.join(rootDir, "views"));
    this.set('view engine', 'jade');
    this.use(express.favicon());
    this.use(expressLogger);
    this.use(express.cookieParser());
    this.use(bodyParser.urlencoded({
      extended: true
    }));
    this.use(bodyParser.json());
    this.use(express.session({
      secret: "carpe diem"
    }));
    this.use(this.router);
    this.use(serveStatic('public'));
    return this.use(express.errorHandler());
  };

}).call(this);

//# sourceMappingURL=all.map
