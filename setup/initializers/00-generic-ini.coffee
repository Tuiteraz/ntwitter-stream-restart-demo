#--( DEPENDENCIES
log           = require("helpers/winston-wrapper")(module)
config        = require "nconf"
path          = require "path"
#--) DEPENDENCIES

module.exports = ()->
  config.file {"file": path.join(process.cwd(),"config.json")}