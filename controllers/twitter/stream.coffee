#--( DEPENDENCIES

log          = require("helpers/winston-wrapper")(module)
config       = require "nconf"

url          = require 'url'
util         = require 'util'
_            = require "underscore"
_.str        = require "underscore.string"
_.mixin _.str.exports()
async        = require "async"
qs           = require "qs"
request      = require "request"
moment       = require "moment"
geocoder     = require "geocoder"

Twitter      = require "ntwitter"

#--) DEPENDENCIES

bIsConnected = false
aRulesQueue = []

jTw = new Twitter {
  consumer_key        : "lvC7DY6OX6g8jlrswqW2RStbC"
  consumer_secret     : "274xqECT5H1TgOXtt594v5TsYUQ3vQZfZ6s5wVMMPrgqQUFF3n"
  access_token_key     : "14489493-3wdBmpI1KUNJWFqpKcrsgLHZbnm6cgBS0MOIA9EpO"
  access_token_secret : "2IwcXxyM8m2Yehs6i0noK8KD2KrNMYJX5QfDqpgYFc8Mr"
}

createStream = (hParams,jApp)->
  sLogHeader = "[TwStream]"
  log.info "#{sLogHeader} stream params:#{JSON.stringify(hParams)}"
  log.info "#{sLogHeader} creating new stream .."

  bFirstTweetReceived = false
  jTw.stream "statuses/filter", hParams, (jStream)->
    jApp.jStream = jStream
    jApp.jStream.bEnabled = true

    jStream.on('data', (hData)->
      return if _.isNull(hData) || jStream.bEnabled == false

      if !bFirstTweetReceived
        log.info "#{sLogHeader} data transfer started!"
        bFirstTweetReceived = true

      bSkipThis = false
      bSkipThis = true if (hParams.locations.length !=0) && _.isNull(hData.place)

      if !bSkipThis
        sLocation = hData.place.full_name if _.isObject(hData.place)
        sData = "[stream-data] @#{hData.user.screen_name}: #{hData.text} [#{hData.created_at}] - #{sLocation}"
        log.info sData

    )
    jStream.on 'ready',()->
      log.info "#{sLogHeader} Twitter stream READY!"
    jStream.on 'open',()->
      log.info "#{sLogHeader} Twitter stream OPEN!"


    # err can be hash and string
    jStream.on 'error',(err,iStatusCode)->
      log.error "#{sLogHeader} stream error:#{iStatusCode}-#{err}"
      bFirstTweetReceived = false

    jStream.on 'fail',()->
      log.error "#{sLogHeader} stream fail"

    jStream.on 'destroy',()->
      log.info " stream destroyed"
      jApp.jStream = null

onNewQuery = (sQuery,jApp) ->
  sLogHeader = ".onNewQuery()"
  log.info "#{sLogHeader} emitted by client. QUERY: #{sQuery}"

  hQuery = JSON.parse(sQuery)

  if _.isObject jApp.jStream
    jApp.jStream.bEnabled = false
    jApp.jStream.destroy()

  async.series [
    (fnNext)->
      if !_.isEmpty hQuery.sCountryCode
        sLocation = ""
        sLocation += "#{hQuery.sState}, "  if !_.isEmpty hQuery.sState
        sLocation += hQuery.sCountryCode
        geocoder.geocode sLocation, (jErr, hData)->
          log.error "#{sLogHeader}.geocode(#{sLocation}) error: #{JSON.stringify(jErr)}" if jErr
          log.info "#{sLogHeader}.geocode(#{sLocation}) result : #{JSON.stringify(hData)}" if hData
          if (hData.status =="OK") && (hData.results.length > 0)
            hNE = hData.results[0].geometry.bounds.northeast
            hSW = hData.results[0].geometry.bounds.southwest
            hQuery.sLocation = "#{hSW.lng}, #{hSW.lat}, #{hNE.lng}, #{hNE.lat}"
          else
            hQuery.sLocation = ""

          fnNext()
      else
        hQuery.sLocation = ""
        fnNext()
    (fnNext)->
      hStreamParams =
        track     : hQuery.sTrack
        language  : if hQuery.sLanguage then hQuery.sLanguage else 'en'
        locations : hQuery.sLocation

      createStream hStreamParams, jApp

      fnNext()
  ],(err,data)->




exports.onNewQuery = onNewQuery
exports.getRateLimitStatus = (fnCallback)->
  jTw.rateLimitStatus fnCallback