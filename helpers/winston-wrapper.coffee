#--( DEPENDENCIES
winston = require('winston');
moment  = require "moment"
#--) DEPENDENCIES

fileLog = new winston.Logger {
  transports: [
    new winston.transports.File {
      colorize  : false
      filename  : "log/app.log"
      maxsize   : 108000
      maxFiles  : 3
      json      : false
    }
  ]
}

module.exports = (module)->

  ENV    = process.env.NODE_ENV
  sLevel = if (ENV == 'development') then 'debug' else 'info'
  sPath  = module.filename.split('/').slice(-2).join('/');

  log = new winston.Logger {
    transports: [
      new winston.transports.Console {
        colorize: true
        timestamp: ()->
          moment().format()
      }
    ]
  }

  log.transports.console.level = sLevel
  log.transports.console.label = sPath.grey

  fileLog.transports.file.level = log.transports.console.level
  fileLog.transports.file.label = log.transports.console.label


  log.info('Init'.green)
  fileLog.info('Init'.green)

  return {
    info: ()->
      log.info.apply log,arguments

      fileLog.transports.file.level = log.transports.console.level
      fileLog.transports.file.label = log.transports.console.label

      fileLog.info arguments

    error: ()->
      log.error.apply log,arguments

      fileLog.transports.file.level = log.transports.console.level
      fileLog.transports.file.label = log.transports.console.label

      fileLog.error arguments
  }